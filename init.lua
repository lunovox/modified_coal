local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

dofile(modpath.."/config.lua")
dofile(modpath.."/translate.lua") -- <== Antes de 'api.lua'!
dofile(modpath.."/api.lua")

minetest.log('action',"["..modname:upper().."] Loaded!")
